﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureMove : MonoBehaviour {
    public string craving;
    public float maxSpeed, acceleration;

    private Rigidbody2D rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
		
	}
	
	// Update is called once per frame
	void Update () {
        //movement
        rb.AddForce((getNearestFoodPosition() - transform.position).normalized *  rb.mass * acceleration);
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
	}
    
    private Vector3 getNearestFoodPosition()
    {
        GameObject[] nearbyFood = GameObject.FindGameObjectsWithTag("Food");
        if (nearbyFood.Length > 0)
        {
            Vector3 nearestFoodPos = nearbyFood[0].transform.position; 
            foreach (GameObject food in nearbyFood)
            {
                if (Vector3.Distance(transform.position, nearestFoodPos) > Vector3.Distance(transform.position, food.transform.position))
                    nearestFoodPos = food.transform.position;      
            }
            return nearestFoodPos; 
        }
        return transform.position; 
    }
}
