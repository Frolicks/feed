﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardSpawning : MonoBehaviour {

    public static float XBOUND = 5;

    public GameObject lavaGeyser; 
	
    public void spawnLava()
    {
        Instantiate(lavaGeyser, new Vector3(Random.Range(-XBOUND, XBOUND), -5, 0), Quaternion.identity); 
    }    
}
