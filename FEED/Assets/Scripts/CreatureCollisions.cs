﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class CreatureCollisions : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //death
        if(collision.gameObject.CompareTag("Hazard")) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); 
        }

        //eat
        if(collision.gameObject.CompareTag("Food"))
        {

            Destroy(collision.gameObject); 
        }
    }
}
