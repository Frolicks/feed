﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocitySetter : MonoBehaviour {
    public Vector3 velocity;
    private void Update()
    {
        GetComponent<Rigidbody2D>().velocity = velocity * Time.deltaTime;
    }
}
