﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardSpawner : MonoBehaviour {

    private HazardSpawning hazards;

    private void Start()
    {
        hazards = GetComponent<HazardSpawning>();
        hazards.InvokeRepeating("spawnLava", 2, 2);
    }

    private void Update()
    {

    }
}
