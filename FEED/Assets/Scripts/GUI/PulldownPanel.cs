﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
using UnityEngine.EventSystems; 
using UnityEngine.UI;

//only configured for bottom-up movement
public class PulldownPanel : MonoBehaviour, IDragHandler, IEndDragHandler  {
    public float maxY, fallRate;

    private RectTransform InventoryPos;
    private float closedPos;
    private bool locked; 
   
    public void Start()
    {
        InventoryPos = transform.parent.GetComponent<RectTransform>();
        closedPos = InventoryPos.anchoredPosition.y; 
    }

    public void Update()
    {
        if (!locked)
        {
            if (InventoryPos.anchoredPosition.y > closedPos + 3) // +3 to prevent it < closedPos and locking forever
                InventoryPos.anchoredPosition -= Vector2.up * fallRate * Time.deltaTime; 
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        locked = true; 
        if (eventData.position.y < maxY && eventData.position.y >= closedPos)
            InventoryPos.anchoredPosition = new Vector2(0, eventData.position.y); 
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (eventData.position.y < maxY - 5)
            locked = false; 
    }

}
