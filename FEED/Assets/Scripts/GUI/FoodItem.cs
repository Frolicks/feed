﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FoodItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler 
{
    private RectTransform rt;
    private Rect inventoryPanel;
    private Vector2 startPos;

    public GameObject worldVersion; 

    public void Start()
    {
        rt = GetComponent<RectTransform>();
        inventoryPanel = GameObject.Find("Inventory Panel").GetComponent<RectTransform>().rect;
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        GetComponent<CanvasGroup>().blocksRaycasts = false;
        startPos = rt.position; 
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 mousePos;
        RectTransformUtility.ScreenPointToWorldPointInRectangle(rt, eventData.position, null, out mousePos);
        rt.position = mousePos;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //WORLD CHECK
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(eventData.position), Vector2.zero);

        bool inAir = false; 
        if (hit.collider == null)
        {
            inAir = true; 
        }

        //CANVAS CHECK
        List<RaycastResult> underMe = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, underMe);
        bool onInventory = false; 
        foreach(RaycastResult r in underMe)
        {
            if (r.gameObject.name == "Inventory Panel")
                onInventory = true; 
        }

        if(!onInventory)
        {
            if(inAir)
            {
                Instantiate(worldVersion, Camera.main.ScreenToWorldPoint(eventData.position) + Vector3.forward * 10f, Quaternion.identity);
                Destroy(gameObject);
            } else
            {
                rt.position = startPos; 
            }
        } 
      
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

}